var eejs     = require('ep_etherpad-lite/node/eejs/');
var settings = require('ep_etherpad-lite/node/utils/Settings');

exports.eejsBlock_mySettings = function (hook_name, args, cb) {
    if (settings.skinName === 'colibris') {
        args.content = args.content + eejs.require('ep_colibris_full_width/templates/colibris_full_width.ejs');
        return cb();
    }
}
