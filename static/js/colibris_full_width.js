exports.aceEditorCSS = function(hook_name, cb){
    return ["/ep_colibris_full_width/static/css/iframe.css"];
}
exports.postAceInit = function(hook_name, cb) {
    if (typeof(toggleColibriFullWidth) === 'function') {
        toggleColibriFullWidth();
    }
}
